package zeroring

import (
	"sync"
	"time"
)

type ZeroRingMetrics struct {
	intervalStart    time.Time     // start of the measurement period
	intervalDuration time.Duration // duration of the measurement
	msgsSent         int64         // messages successfully sent during the period
	msgsDiscarded    int64         // messages discarded during the period
	bytesSent        int64         // bytes successfully send during the period
	bytesDiscarded   int64         // bytes discarded during the period
	lock             *sync.RWMutex // lock for syncronization
}

// NewZeroRingMetrics creates a new instance
func NewZeroRingMetrics() *ZeroRingMetrics {
	m := new(ZeroRingMetrics)
	m.msgsSent = 0
	m.msgsDiscarded = 0
	m.bytesSent = 0
	m.bytesDiscarded = 0
	m.intervalStart = time.Now()
	m.lock = &sync.RWMutex{}
	return m
}

// IncrMsgsSent increments the msgsSent counter by amt
func (m *ZeroRingMetrics) IncrMsgsSent(amt int64) {
	m.lock.Lock()
	m.msgsSent += amt
	m.lock.Unlock()
}

// IncrMsgsDiscarded increments the msgsDiscarded counter by amt
func (m *ZeroRingMetrics) IncrMsgsDiscarded(amt int64) {
	m.lock.Lock()
	m.msgsDiscarded += amt
	m.lock.Unlock()
}

// IncrBytesSent increments the bytesSent counter by amt
func (m *ZeroRingMetrics) IncrBytesSent(amt int64) {
	m.lock.Lock()
	m.bytesSent += amt
	m.lock.Unlock()
}

// IncrBytesDiscarded increments the bytesDiscarded counter by amt
func (m *ZeroRingMetrics) IncrBytesDiscarded(amt int64) {
	m.lock.Lock()
	m.bytesDiscarded += amt
	m.lock.Unlock()
}

// ResetIntervalStart sets the intervalStart to now
func (m *ZeroRingMetrics) ResetIntervalStart() {
	m.lock.Lock()
	m.intervalStart = time.Now()
	m.lock.Unlock()
}

// ResetZeroRingMetrics resets the entire metric
func (m *ZeroRingMetrics) ResetZeroRingMetrics() {
	m.lock.Lock()
	m.msgsSent = 0
	m.msgsDiscarded = 0
	m.bytesSent = 0
	m.bytesDiscarded = 0
	m.intervalStart = time.Now()
	m.lock.Unlock()
}

// MsgsSent returns the number of messages sent during the current interval
func (m *ZeroRingMetrics) MsgsSent() int64 {
	return m.msgsSent
}

// MsgsDiscarded returns the number of messages dropped during the current interval
func (m *ZeroRingMetrics) MsgsDiscarded() int64 {
	return m.msgsDiscarded
}

// BytesSent returns the number of bytes sent during the current interval
func (m *ZeroRingMetrics) BytesSent() int64 {
	return m.bytesSent
}

// BytesDiscarded returns the number of bytes dropped during the current inteval
func (m *ZeroRingMetrics) BytesDiscarded() int64 {
	return m.bytesDiscarded
}

// IntervalStart returns the time the current interval started
func (m *ZeroRingMetrics) IntervalStart() time.Time {
	return m.intervalStart
}

// IntervalDuration returns the duration of the current interval
func (m *ZeroRingMetrics) IntervalDuration() time.Duration {
	return time.Since(m.IntervalStart())
}
