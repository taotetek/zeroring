package zeroring

import (
	"testing"
	"time"
)

// handy compare function - https://github.com/zaphar/go-stem/blob/master/stemmer/stemmer_test.go#L8
func compare(t *testing.T, expected, actual interface{}, msg ...string) {
	if expected != actual {
		t.Errorf("[%v] -- value differs. Expected [%v], actual [%v]", msg, expected, actual)
	}
}

func TestNoDiscard(t *testing.T) {
	inChannel := make(chan string)
	outChannel := make(chan string, 2)
	zRing := NewZeroRing(inChannel, outChannel)
	go zRing.Run()

	inChannel <- "hello"
	compare(t, false, zRing.IsBlocked(), "should not report muted state")

	recvd := <-outChannel
	compare(t, "hello", recvd, "should receive message correctly")
	compare(t, int64(1), zRing.MsgsSent(), "should report 1 message sent")
	compare(t, int64(5), zRing.BytesSent(), "should report 5 bytes sent")
	compare(t, int64(0), zRing.MsgsDiscarded(), "should report 0 messages discarded")
	compare(t, int64(0), zRing.BytesDiscarded(), "should report 0 messages discarded")
	compare(t, time.Now().Round(time.Minute), zRing.IntervalStart().Round(time.Minute))
}

func TestDiscard(t *testing.T) {
	inChannel := make(chan string)
	outChannel := make(chan string, 2)
	zRing := NewZeroRing(inChannel, outChannel)
	go zRing.Run()

	inChannel <- "hello1"
	inChannel <- "hello2"
	compare(t, false, zRing.IsBlocked(), "should not report mute state")

	inChannel <- "hello3"
	compare(t, true, zRing.IsBlocked(), "should report mute state")

	recvd := <-outChannel
	compare(t, true, zRing.IsBlocked(), "should not report mute state")
	compare(t, "hello2", recvd, "should have received second message (first should be discarded")

	recvd = <-outChannel
	compare(t, "hello3", recvd, "should have received third message")

	compare(t, int64(2), zRing.MsgsSent(), "should report 2 messages sent")
	compare(t, int64(12), zRing.BytesSent(), "should report 12 bytes sent")
	compare(t, int64(1), zRing.MsgsDiscarded(), "should report 1 message discarded")
	compare(t, int64(6), zRing.BytesDiscarded(), "should report 6 bytes discarded")
}

func BenchmarkZeroRing(b *testing.B) {
	inChannel := make(chan string)
	outChannel := make(chan string, 2)
	zRing := NewZeroRing(inChannel, outChannel)
	go zRing.Run()

	for i := 0; i < b.N; i++ {
		inChannel <- "hello!"
		_ = <-outChannel
	}
}
