// Package zeroring provides a channel based ringbuffer with monitoring and metrics.
package zeroring

import (
	"log"
	"sync"
	"time"
)

// The default handler for ZeroRing queue full state.  Discards messages.
func defaultFullHandler(z *ZeroRing, msg string) error {
	msgToDiscard := <-z.outChannel
	z.metrics.msgsDiscarded += 1
	z.metrics.bytesDiscarded += int64(len(msgToDiscard))
	z.outChannel <- msg
	return nil
}

type ZeroRing struct {
	inChannel   <-chan string                       // input channel to send data into the ringbuffer
	outChannel  chan string                         // output channel to receive data from the ringbuffer
	lock        *sync.RWMutex                       // lock to coordinate concurrent access
	blocked     bool                                // is the outgoing channel currently blocked
	metrics     *ZeroRingMetrics                    // holds metrics for this ZeroRing instance
	fullHandler func(z *ZeroRing, msg string) error // handler for queue full events
}

// NewZeroRing creates a new ZeroRing given an input and output channel
func NewZeroRing(in <-chan string, out chan string) *ZeroRing {
	z := new(ZeroRing)
	z.inChannel = in
	z.outChannel = out
	z.lock = &sync.RWMutex{}
	z.metrics = NewZeroRingMetrics()
	z.fullHandler = defaultFullHandler
	return z
}

// IsBlocked returns whether the ZeroRing is currently blocked
func (z *ZeroRing) IsBlocked() bool {
	return z.blocked
}

// MsgsSent returns the number of messages sent during the current interval
func (z *ZeroRing) MsgsSent() int64 {
	return z.metrics.MsgsSent()
}

// MsgsDiscarded returns the number of messages dropped during the current interval
func (z *ZeroRing) MsgsDiscarded() int64 {
	return z.metrics.MsgsDiscarded()
}

// BytesSent returns the number of bytes sent during the current interval
func (z *ZeroRing) BytesSent() int64 {
	return z.metrics.BytesSent()
}

// BytesDiscarded returns the number of bytes dropped during the current inteval
func (z *ZeroRing) BytesDiscarded() int64 {
	return z.metrics.BytesDiscarded()
}

// IntervalStart returns the time the current interval started
func (z *ZeroRing) IntervalStart() time.Time {
	return z.metrics.IntervalStart()
}

// IntervalDuration returns the duration of the current interval
func (z *ZeroRing) IntervalDuration() time.Duration {
	return time.Since(z.metrics.IntervalStart())
}

// Run starts the ZeroRing
func (z *ZeroRing) Run() {
	z.metrics.intervalStart = time.Now()
	for msg := range z.inChannel {
		z.lock.Lock()
		select {
		case z.outChannel <- msg:
			z.blocked = false
			z.metrics.msgsSent += 1
			z.metrics.bytesSent += int64(len(msg))
		default:
			z.blocked = true
			err := z.fullHandler(z, msg)
			if err != nil {
				log.Fatal("ALL HANDS ABANDON SHIP")
			}
		}
		z.lock.Unlock()
	}
	close(z.outChannel)
}
