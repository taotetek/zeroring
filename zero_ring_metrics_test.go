package zeroring

import (
	"testing"
	"time"
)

func TestMetrics(t *testing.T) {
	zMetrics := NewZeroRingMetrics()
	compare(t, int64(0), zMetrics.MsgsSent(), "MsgsSent should be initialized to 0")
	compare(t, int64(0), zMetrics.BytesSent(), "BytesSent should be initialized to 0")
	compare(t, int64(0), zMetrics.MsgsDiscarded(), "MsgsDiscarded should be initialized to 0")
	compare(t, int64(0), zMetrics.BytesDiscarded(), "BytesDiscarded should be initialized to 0")
	compare(t, time.Now().Round(time.Minute), zMetrics.IntervalStart().Round(time.Minute))

	zMetrics.IncrMsgsSent(1)
	compare(t, int64(1), zMetrics.MsgsSent(), "IncrMsgsSent should increment msgsSent")

	zMetrics.IncrBytesSent(5)
	compare(t, int64(5), zMetrics.BytesSent(), "IncrBytesSent should increment bytes sent")

	zMetrics.IncrMsgsDiscarded(2)
	compare(t, int64(2), zMetrics.MsgsDiscarded(), "IncrMsgsDiscarded should increment msgsDiscarded")

	zMetrics.IncrBytesDiscarded(10)
	compare(t, int64(10), zMetrics.BytesDiscarded(), "IncrBytesDiscarded should increment bytesDiscarded")

	zMetrics.ResetIntervalStart()
	compare(t, time.Now().Round(time.Minute), zMetrics.IntervalStart().Round(time.Minute))

	zMetrics.ResetZeroRingMetrics()
	compare(t, int64(0), zMetrics.MsgsSent(), "MsgsSent should be initialized to 0")
	compare(t, int64(0), zMetrics.BytesSent(), "BytesSent should be initialized to 0")
	compare(t, int64(0), zMetrics.MsgsDiscarded(), "MsgsDiscarded should be initialized to 0")
	compare(t, int64(0), zMetrics.BytesDiscarded(), "BytesDiscarded should be initialized to 0")
	compare(t, time.Now().Round(time.Minute), zMetrics.IntervalStart().Round(time.Minute))
}
